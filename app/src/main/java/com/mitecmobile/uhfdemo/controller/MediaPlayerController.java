package com.mitecmobile.uhfdemo.controller;

import android.content.Context;
import android.media.MediaPlayer;

import com.mitecmobile.uhfdemo.activity.ConfigActivity;
import com.mitecmobile.uhfdemo.utils.SharedPreferencesUtil;

public class MediaPlayerController {

    private static MediaPlayerController instance;
    private MediaPlayer mediaPlayer = null;
    private Context context;
    private int rawID = 0;

    private MediaPlayerController() {
    }

    public static MediaPlayerController getInstance() {
        if (instance == null)
            instance = new MediaPlayerController();
        return instance;
    }

    public void initPlayer(Context contxt, int resId) {
        this.context = contxt;
        this.rawID = resId;
    }

    public void play() {
        if (context == null) {
            return;
        }

        if (rawID == 0) {
            return;
        }

        release();
        if (SharedPreferencesUtil.checkIfKeyExists(ConfigActivity.KEY_BEEP) && SharedPreferencesUtil.loadObject(ConfigActivity.KEY_BEEP, Boolean.class)) {
            mediaPlayer = MediaPlayer.create(context, rawID);
            mediaPlayer.start();
        }
    }

    public void release() {
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

}
