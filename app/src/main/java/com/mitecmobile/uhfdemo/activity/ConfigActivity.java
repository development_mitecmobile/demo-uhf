package com.mitecmobile.uhfdemo.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.SeekBar;

import com.mitecmobile.uhfdemo.R;
import com.mitecmobile.uhfdemo.databinding.ActivityConfigBinding;
import com.mitecmobile.uhfdemo.utils.SharedPreferencesUtil;
import com.senter.iot.support.openapi.uhf.UhfD2;

import java.util.ArrayList;



public class ConfigActivity extends ScannerActivity implements SeekBar.OnSeekBarChangeListener, CompoundButton.OnCheckedChangeListener {

    private ActivityConfigBinding mBinding;
    public static final String KEY_POWER = "key_power";
    public static final String KEY_BEEP = "key_beep";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_config);
        mBinding.scannerPowerSelector.setOnSeekBarChangeListener(this);
        mBinding.beepOnScan.setOnCheckedChangeListener(this);
        initDevice();

        if (SharedPreferencesUtil.checkIfKeyExists(KEY_POWER)) {
            int progress = SharedPreferencesUtil.loadObject(KEY_POWER, Integer.class);
            mBinding.scannerPowerSelector.setProgress(progress);
        }

        if (SharedPreferencesUtil.checkIfKeyExists(KEY_BEEP))
            mBinding.beepOnScan.setChecked(SharedPreferencesUtil.loadObject(KEY_BEEP, Boolean.class));
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        mBinding.scannerPower.setText(String.valueOf(progress));
        SharedPreferencesUtil.saveObject(KEY_POWER, progress);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        //no utilizado
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        //no utilizado
    }

    @Override
    public void onScanSuccess(ArrayList<UhfD2.UII> uids) {
        //autogenerado
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        SharedPreferencesUtil.saveObject(KEY_BEEP, b);
    }
}
