package com.mitecmobile.uhfdemo.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import com.mitecmobile.uhfdemo.R;
import com.mitecmobile.uhfdemo.controller.MediaPlayerController;
import com.mitecmobile.uhfdemo.databinding.ActivityLecturaBinding;
import com.mitecmobile.uhfdemo.scanner.ScannerError;
import com.mitecmobile.uhfdemo.utils.Utils;
import com.senter.iot.support.openapi.uhf.UhfD2;

import java.util.ArrayList;

public class LecturaActivity extends ScannerActivity {

    private ActivityLecturaBinding mBinding;
    private ArrayList<String> dataList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_lectura);
        dataList = new ArrayList<>();
        initDevice();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_clean, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_clean) {
            dataList = new ArrayList<>();
            buildResultados(dataList);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //Código del gatillo
        if (keyCode == KeyEvent.KEYCODE_WAKEUP ||keyCode == KeyEvent.KEYCODE_F1 ||keyCode == KeyEvent.KEYCODE_F2) {
            launchScan();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void launchScan() {
        switch (mBinding.selectorLectura.getCheckedRadioButtonId()) {
            case R.id.selector_all:
                findAllTags();
                break;
            case R.id.selector_reserved:
                readUHFMemory(UhfD2.Bank.Reserved);
                break;
            case R.id.selector_tid:
                readUHFMemory(UhfD2.Bank.TID);
                break;
            case R.id.selector_user:
                readUHFMemory(UhfD2.Bank.User);
                break;
            case R.id.selector_uii:
                readUHFMemory(UhfD2.Bank.UII);
                break;
            default:
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    /**
     * Lee la memoria indicada
     *
     * @param bank banco de memoria a leer
     */

    private void readUHFMemory(UhfD2.Bank bank) {
        int posicionInicial = 2;//getIntFromEditText(mBinding.scannerReadPosition);
        int longitud = 6;//getIntFromEditText(mBinding.scannerReadLength);
        getInterrogatorModel().iso18k6cRead(getDefaultAccessPasword(), bank, posicionInicial, longitud, new UhfD2.UmdOnIso18k6cRead() {
            @Override
            public void onTagRead(int i, UhfD2.UII uii, byte[] bytes, UhfD2.UmdFrequencyPoint umdFrequencyPoint, Integer integer, int i1) {
                String formatedUII = Utils.xGetString(bytes);
                addItemToList(formatedUII);
            }

            @Override
            public void onFailed(UhfD2.UmdErrorCode umdErrorCode) {
                Log.e("Scanner error", umdErrorCode.name());
                showError(ScannerError.getScannerErrorByByteID(umdErrorCode.getErrorCodeAsByte()));
            }
        });
    }

    private int getIntFromEditText(EditText editText) {
        String text = editText.getText().toString();
        if (text != null && !text.isEmpty()) {
            return Integer.valueOf(text);
        }
        return 0;
    }

    @Override
    public void onScanSuccess(ArrayList<UhfD2.UII> uids) {
        ArrayList<String> uidsFormateados = new ArrayList<>();
        for (UhfD2.UII uii : uids)
            uidsFormateados.add(Utils.xGetString(uii.getBytes()));
        showScanResult(uidsFormateados);
    }

    private void addItemToList(final String item){
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!dataList.contains(item))
                    dataList.add(item);
                showScanResult(dataList);
            }
        });
    }

    private void showScanResult(final ArrayList<String> uids) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                buildResultados(uids);
            }
        });
    }

    private void buildResultados(ArrayList<String> uids) {
        MediaPlayerController.getInstance().play();
        ArrayAdapter<String> adaptador =
                new ArrayAdapter(this,
                        android.R.layout.simple_list_item_1, uids);
        mBinding.listado.setAdapter(adaptador);
    }

}
