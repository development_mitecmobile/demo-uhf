package com.mitecmobile.uhfdemo.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.widget.TextView;
import android.widget.Toast;

import com.mitecmobile.uhfdemo.R;
import com.senter.support.openapi.StBarcodeScanner;

import java.io.UnsupportedEncodingException;

public class BarcodeActivity extends AppCompatActivity {

    private StBarcodeScanner scanner;
    private boolean isScanning;
    private TextView textView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);
        scanner = StBarcodeScanner.getInstance();
        if (scanner == null) {
            Toast.makeText(this, R.string.error_scanner_null, Toast.LENGTH_SHORT).show();
            this.finish();
        }
        textView = findViewById(R.id.scan_text);
    }

    private boolean isScannerActive() {
        return scanner != null;
    }

    @Override
    protected void onDestroy() {
        if (!isScannerActive())
            scanner.uninit();
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_WAKEUP ||keyCode == KeyEvent.KEYCODE_F1 ||keyCode == KeyEvent.KEYCODE_F2) {
            scanItem();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    private void scanItem() {
        if (isScanning)
            return;
        isScanning = true;
        new Thread() {
            @Override
            public void run() {
                try {
                    StBarcodeScanner.BarcodeInfo rslt = scanner.scanBarcodeInfo();//scan ,if failed,null will be return
                    String result = null;
                    if (rslt != null)
                        result = new String(rslt.getBarcodeValueAsBytes(), "utf-8");
                    handleScanMessage(result);
                } catch (InterruptedException e) {
                    showError();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } finally {
                    isScanning = false;
                }
            }
        }.start();
    }

    private void handleScanMessage(final String message) {
        if(message != null)
            runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setText(message);
            }
        });
    }

    private void showError() {
        Toast.makeText(this, R.string.error_scan_interrupted, Toast.LENGTH_SHORT).show();
    }
}
