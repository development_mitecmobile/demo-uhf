package com.mitecmobile.uhfdemo.activity;

import android.app.AlertDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.mitecmobile.uhfdemo.R;
import com.mitecmobile.uhfdemo.adapter.DialogListAdapter;
import com.mitecmobile.uhfdemo.adapter.OnDataClickListener;
import com.mitecmobile.uhfdemo.databinding.ActivityEscrituraBinding;
import com.mitecmobile.uhfdemo.scanner.ScannerError;
import com.mitecmobile.uhfdemo.utils.Utils;
import com.senter.iot.support.openapi.uhf.UhfD2;

import java.util.ArrayList;

public class EscrituraActivity extends ScannerActivity implements OnDataClickListener {

    private ActivityEscrituraBinding mBinding;
    private AlertDialog selectorUidDialog;
    private UhfD2.UII selectedTag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_escritura);
        initDevice();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_WAKEUP || keyCode == KeyEvent.KEYCODE_F1 || keyCode == KeyEvent.KEYCODE_F2) {
            writeInTag();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void writeInTag() {
        String message = mBinding.message.getText().toString();
        String stringPosition = mBinding.scannerWritePosition.getText().toString();
        int position = 0;

        if (message == null || message.isEmpty()) {
            Toast.makeText(this, "Introduce un mensaje", Toast.LENGTH_SHORT).show();
            return;
        } else if (message.length() % 4 != 0) {
            Toast.makeText(this, "El mensaje debe tener una longitud múltiplo de 4", Toast.LENGTH_SHORT).show();
            return;
        }

        if (stringPosition != null && !stringPosition.isEmpty()) {
            position = Integer.valueOf(stringPosition);
        }

        if (!shouldAllowWrite()) {
            showError(ScannerError.ScannerConfigurationError);
            return;
        }

        UhfD2.Bank destino = getBank();
        if (destino != null) {
            byte[] messageInBytes = Utils.getBytesByHexString(message);
            for (byte b : messageInBytes)
                Log.e("onTagWrite", "" + b);

            getInterrogatorModel().iso18k6cWrite(getDefaultAccessPasword(), destino, position, messageInBytes, new UhfD2.UmdOnIso18k6cWrite() {
                @Override
                public void onTagWrite(int i, UhfD2.UII uii, UhfD2.UmdErrorCode umdErrorCode, UhfD2.UmdFrequencyPoint umdFrequencyPoint, Integer integer, int i1) {
                    Log.e("onTagWrite", umdErrorCode.name());
                    if (umdErrorCode == UhfD2.UmdErrorCode.command_success)
                        onWriteSuccess();
                    else
                        onFailed(umdErrorCode);
                }

                @Override
                public void onFailed(UhfD2.UmdErrorCode umdErrorCode) {
                    showError(ScannerError.getScannerErrorByByteID(umdErrorCode.getErrorCodeAsByte()));
                }
            });
        }
    }

    private void onWriteSuccess() {
        showSuccess();
        if (isSpecificTagSelected()) {
            selectedTag = null;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mBinding.modoScanner.setText(getText(R.string.scanner_default));
                }
            });
        }
    }

    private UhfD2.Bank getBank() {
        switch (mBinding.selectorEscritura.getCheckedRadioButtonId()) {
            case R.id.selector_reserved:
                return UhfD2.Bank.Reserved;
            case R.id.selector_uii:
                return UhfD2.Bank.UII;
            case R.id.selector_tid:
                return UhfD2.Bank.TID;
            case R.id.selector_user:
                return UhfD2.Bank.User;
            default:
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
                return null;
        }
    }

    public void onClickSearchTags(View view) {
        findAllTags();
    }

    @Override
    public void onScanSuccess(final ArrayList<UhfD2.UII> uids) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                createDialog(createDialogView(uids));
            }
        });
    }

    private void createDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setTitle(R.string.app_name);
        builder.setNeutralButton("Cancelar", null);
        selectorUidDialog = builder.create();
        selectorUidDialog.show();
    }

    private View createDialogView(ArrayList<UhfD2.UII> values) {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_tag_selector, null);
        DialogListAdapter adapter = new DialogListAdapter(values);
        adapter.setmOnDataClickListener(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this,
                LinearLayoutManager.VERTICAL);
        RecyclerView recyclerView = view.findViewById(R.id.selector_tag_id);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        return view;
    }

    @Override
    public void onDataClick(UhfD2.UII data) {
        selectedTag = data;
        mBinding.modoScanner.setText(getString(R.string.scanner_lock, Utils.xGetString(data.getBytes())));
        if (selectorUidDialog.isShowing())
            selectorUidDialog.dismiss();
    }

    /**
     * Si se ha seleccionado una tag en concreto configura el escaner para que funcione únicamente con esa.
     * Si no se ha seleccionado ninguna devuelve true, permitiendo que el escaner actúe sobre la primera que encuentre.
     *
     * @return el resultado de la configuración
     */

    private boolean shouldAllowWrite() {
        if (isSpecificTagSelected()) {
            UhfD2.UmdEpcMatchSetting epcMatchSetting = UhfD2.UmdEpcMatchSetting.newInstanceOfMatchingEpcFieldInUii(selectedTag.getEpc().getBytes());
            return getInterrogatorModel().iso18k6cSetAccessEpcMatch(epcMatchSetting);
        } else
            return true;

    }

    public boolean isSpecificTagSelected() {
        return selectedTag != null;
    }
}
