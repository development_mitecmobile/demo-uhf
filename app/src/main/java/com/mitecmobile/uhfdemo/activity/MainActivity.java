package com.mitecmobile.uhfdemo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.mitecmobile.uhfdemo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClickLectura(View view) {
        startActivity(new Intent(this, LecturaActivity.class));
    }

    public void onClickEscritura(View view) {
        startActivity(new Intent(this, EscrituraActivity.class));
    }

    public void onClickBarcode(View view) {
        startActivity(new Intent(this, BarcodeActivity.class));
    }

    public void onClickConfig(View view) {
        startActivity(new Intent(this, ConfigActivity.class));
    }
}
