package com.mitecmobile.uhfdemo.activity;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.mitecmobile.uhfdemo.R;
import com.mitecmobile.uhfdemo.controller.MediaPlayerController;
import com.mitecmobile.uhfdemo.scanner.OnContinuousScanItem;
import com.mitecmobile.uhfdemo.scanner.ScannerError;
import com.mitecmobile.uhfdemo.utils.SharedPreferencesUtil;
import com.mitecmobile.uhfdemo.utils.Utils;
import com.senter.iot.support.openapi.uhf.UhfD2;

import java.util.ArrayList;

/**
 * Clase que aglutina la lógica común del escaner
 */

public abstract class ScannerActivity extends AppCompatActivity implements OnContinuousScanItem {

    private UhfD2 uhf;
    private static final int DEFAULT_POWER = 17; //Potencia con la que la antena detecta tags de forma cómoda
    protected static final String KEY_POWER = "key_power";

    protected void initDevice() {
        uhf = UhfD2.getInstance();
        if (uhf == null) {
            Toast.makeText(this, "No se ha encontrado un hardware compatible", Toast.LENGTH_SHORT).show();
            finish();
        }
        if (uhf.init()) {
            int power = DEFAULT_POWER;
            if (SharedPreferencesUtil.checkIfKeyExists(KEY_POWER)) {
                power = SharedPreferencesUtil.loadObject(KEY_POWER, Integer.class);
            }
            setPower(power);
        }
    }

    /**
     * Getter para más comodidad a la hora de acceder a sus métodos
     */
    protected UhfD2 getInterrogatorModel() {
        return uhf;
    }

    /**
     * Setea el valor de potencia que usará el escaner.
     * Algunos campos de memoria pueden requerir una potencia más alta
     *
     * @param power valor numérico entre 0 (mínimo) y 10 (máximo)
     */

    protected void setPower(int power) {
        uhf.setOutputPower(convertPower(power));
    }

    /**
     * Transforma una escala 1 - 10 a la que usa el escaner
     * Para la correcta y cómoda lectura de tags la potencia mínima debería ser 17
     */

    private static int convertPower(int power) {
        int newPower = (int) ((power * 1.6) + 17);
        if(newPower > 32)
            newPower = 32;
        return newPower;
    }

    /**
     * Es conveniente liberar el escaner al acabar para que otras apps que lo usen no den error
     */
    @Override
    protected void onDestroy() {
        if (uhf != null)
            uhf.uninit();
        super.onDestroy();
    }

    protected void showSuccess() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showSuccessOnUI();
            }
        });
    }

    private void showSuccessOnUI() {
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("Éxito");
        ab.setMessage("Se ha guardado con éxito");
        ab.setPositiveButton("Aceptar", null);
        ab.create().show();
    }

    protected void showError(final ScannerError error) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showErrorOnUI(error);
            }
        });
    }

    private void showErrorOnUI(ScannerError error) {
        AlertDialog.Builder ab = new AlertDialog.Builder(this);
        ab.setTitle("Error");
        ab.setMessage(error.getStringId());
        ab.setPositiveButton("Aceptar", null);
        ab.create().show();
    }

    /**
     * Devuelve la contraseña de acceso por defecto
     */

    public UhfD2.AccessPassword getDefaultAccessPasword() {
        byte[] passBytes = Utils.getBytesByHexString("00000000"); //Contraseña de acceso por defecto
        return UhfD2.AccessPassword.getNewInstance(passBytes);
    }

    /**
     * Detecta todas las tags cercanas
     */

    public void findAllTags() {
        final ArrayList<UhfD2.UII> uids = new ArrayList<>();
        MediaPlayerController.getInstance().initPlayer(this, R.raw.tag_inventoried);
        getInterrogatorModel().iso18k6cRealTimeInventory(1, new UhfD2.UmdOnIso18k6cRealTimeInventory() {
            @Override
            public void onTagInventory(UhfD2.UII uii, UhfD2.UmdFrequencyPoint umdFrequencyPoint, Integer integer, UhfD2.UmdRssi umdRssi) {
                if (!uids.contains(uii)) {
                    uids.add(uii);
                }
            }

            @Override
            public void onFinishedSuccessfully(Integer integer, int i, int i1) {
                if (!uids.isEmpty()) {
                    onScanSuccess(uids);
                } else {
                    showError(ScannerError.NoTagError);
                }
            }

            @Override
            public void onFinishedWithError(UhfD2.UmdErrorCode umdErrorCode) {
                showError(ScannerError.getScannerErrorByByteID(umdErrorCode.getErrorCodeAsByte()));
            }
        });
    }

}
