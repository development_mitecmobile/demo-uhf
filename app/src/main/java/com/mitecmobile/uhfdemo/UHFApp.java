package com.mitecmobile.uhfdemo;

import android.app.Application;

import com.mitecmobile.uhfdemo.utils.SharedPreferencesUtil;

public class UHFApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferencesUtil.setAppContext(getApplicationContext());
    }
}
