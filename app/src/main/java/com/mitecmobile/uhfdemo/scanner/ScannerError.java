package com.mitecmobile.uhfdemo.scanner;

public enum ScannerError {

    AccessError(55) {
        @Override
        public String getStringId() {
            return "Error de acceso. Revise la contraseña";
        }
    },
    WriteError(51) {
        @Override
        public String getStringId() {
            return "Error de escritura";
        }
    },
    NoTagError(54) {
        @Override
        public String getStringId() {
            return "No se ha encontrado una tag";
        }
    },
    TagSelectedNotFoundError(54) {
        @Override
        public String getStringId() {
            return "No se ha encontrado la tag seleccionada";
        }
    },
    ScannerUnknownError(-1) {
        @Override
        public String getStringId() {
            return "Error desconocido";
        }
    },
    ScannerTagMismatch(-2) {
        @Override
        public String getStringId() {
            return "La tag seleccionada y la detectada no coinciden";
        }
    },
    ScannerConfigurationError(-3) {
        @Override
        public String getStringId() {
            return "Error de configuración";
        }
    };

    private final byte mId;

    ScannerError(int code) {
        this.mId = (byte) code;
    }

    public byte getId() {
        return mId;
    }

    public abstract String getStringId();

    public static ScannerError getScannerErrorByByteID(int id) {
        byte byteId = (byte) id;
        for (ScannerError scannerError : values())
            if (scannerError.getId() == byteId)
                return scannerError;
        return ScannerUnknownError;
    }
}
