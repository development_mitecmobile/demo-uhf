package com.mitecmobile.uhfdemo.scanner;

import com.senter.iot.support.openapi.uhf.UhfD2;

import java.util.ArrayList;

/**
 * Created by Victor on 23/01/2018.
 */

public interface OnContinuousScanItem {

    void onScanSuccess(ArrayList<UhfD2.UII> uids);
}
