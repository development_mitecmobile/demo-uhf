package com.mitecmobile.uhfdemo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mitecmobile.uhfdemo.R;
import com.mitecmobile.uhfdemo.utils.Utils;
import com.mitecmobile.uhfdemo.databinding.RowSelectTagUidBinding;
import com.senter.iot.support.openapi.uhf.UhfD2;

import java.util.List;


public class DialogListAdapter extends RecyclerView.Adapter {

    private List<UhfD2.UII> data;
    private OnDataClickListener mOnDataClickListener;

    public DialogListAdapter(List<UhfD2.UII> data) {
        this.data = data;
    }

    public void setmOnDataClickListener(OnDataClickListener mOnDataClickListener) {
        this.mOnDataClickListener = mOnDataClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_select_tag_uid, parent, false);
        return new DialogListViewHolder(view, mOnDataClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((DialogListViewHolder) holder).bindData(data.get(position));
    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class DialogListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RowSelectTagUidBinding mBinding;
        private OnDataClickListener listener;
        private UhfD2.UII value;

        public DialogListViewHolder(View itemView, OnDataClickListener listener) {
            super(itemView);
            mBinding = RowSelectTagUidBinding.bind(itemView);
            this.listener = listener;
            mBinding.rowUidSelection.setOnClickListener(this);
        }

        public void bindData(UhfD2.UII data) {
            value = data;
            mBinding.setViewHolder(this);
        }

        public String getEPC() {
            return Utils.xGetString(value.getBytes());
        }

        @Override
        public void onClick(View view) {
            if (listener != null)
                listener.onDataClick(value);
        }
    }
}
