package com.mitecmobile.uhfdemo.adapter;


import com.senter.iot.support.openapi.uhf.UhfD2;

public interface OnDataClickListener {

    void onDataClick(UhfD2.UII data);
}
