package com.mitecmobile.uhfdemo.utils;

public class Utils {

    private Utils() {
        //clase no instanciable
    }

    public static byte[] getBytesByHexString(String string) {
        String cleanString = string.replaceAll(" ", "");// delete spaces
        int len = cleanString.length();
        if (len % 2 == 1) {
            return new byte[0];
        }
        byte[] ret = new byte[len / 2];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = (byte) (Integer.valueOf(cleanString.substring(i * 2, i * 2 + 2), 16) & 0xff);
        }
        return ret;
    }

    public static String xGetString(byte[] bs) {
        if (bs != null) {
            StringBuilder sBuffer = new StringBuilder();
            for (int i = 0; i < bs.length; i++) {
                sBuffer.append(String.format("%02x", bs[i]));
            }
            return sBuffer.toString();
        }
        return "";
    }

    public static int hexToDecimal(String hexString){
        return Integer.parseInt(hexString.trim(), 16);
    }
}
