## Resumen para la integración

### Añadir las librerías necesarias
Las encontrarás en el directorio app/libs. Para trabajar con UHF son necesarias las dos.

### ScannerActivity es la clase básica que tiene todo lo necesario para una app sencilla de lectura.
Dispone de los métodos para iniciar y desconectar el lector, fijar la potencia de lectura y detectar todas las tags cercanas.

### LecturaActivity y EscrituraActivity expanden la lógica.
Te permitirán fijar como objetivo una única tag y acceder/editar sus bancos de memoria.
Ten en cuenta que el lector no está diseñado para la escritura de tags, pero hace un apaño.

### BarcodeActivity como complemento del lector.
Integra la lógica del escáner del código de barras. Si el lector es 2D puede que sea necesario el permiso de acceso a la cámara.